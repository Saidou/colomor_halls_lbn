---
output: 
  pdf_document: 
papersize: a4
geometry: "left=2.0cm, right=1.0cm, top=2.5cm, bottom=2.0cm"
header-includes:
  - \usepackage{booktabs}
  - \usepackage{float}
  - \usepackage{tabu}
  - \renewcommand{\familydefault}{\sfdefault}
---
\setlength{\unitlength}{1cm}

\begin{picture}(0,0)(-13, -1)
\includegraphics[height = 1.1 cm]{logo/LogoCermel.png}
\end{picture}

\vspace{0.5cm}

\begin{center}
\Large{\textbf{Estimated excess mortality due to COVID-19 \\ based on data from Town Halls}}
\end{center}

\vspace{0.5cm}

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning = FALSE, error = FALSE, message = FALSE)
options(knitr.kable.NA = "-")
library(dplyr)
library(knitr)
library(kableExtra)

```


```{r figure0, fig.cap="Daily confirmed cases of COVID19 (7-day average)", fig.pos='h', fig.height=4.5, fig.width=8}
fig_0
```


```{r}
tab_1 %>% 
  kable(booktabs = T, format = "latex", caption = "Distribution of deaths according to age and gender",
        col.names = c(" ", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020"),
        align = c("l", "r", "r", "r", "r", "r", "r", "r", "r", "r", "r"), format.args = list(big.mark = ' ')) %>%
  kable_styling(latex_options = c("HOLD_position", "scale_down")) %>%
  row_spec(row = 0, bold = TRUE) %>%
  pack_rows(" ", 1, 1) %>%
  pack_rows("Age group", 2, 4) %>%
  pack_rows("Gender", 5, 7)
```

```{r}
tab_2 %>%
  kable(caption = "Monthly deaths per year", 
        format = "latex", booktabs = TRUE, linesep = "",
        col.names = c(" ", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020",
                      "mean[note]", "Excess[note]", "p-score"),
        format.args = list(big.mark = ' '), align = c("l", "r", "r", "r", "r", "r", "r", "r", "r", "r", "r", "r", "r", "r")) %>%
  kable_styling(latex_options = c("HOLD_position", "scale_down")) %>%
  row_spec(c(0, nrow(tab_2)), bold = T) %>%
  column_spec(1, bold = T) %>%
  pack_rows(" ", nrow(tab_2), nrow(tab_2)) %>%
  add_footnote(c("Average deaths from 2011 to 2019.",
                 "Number of excess deaths in 2020."), notation = "number")
```

\vspace{1.0cm}

```{r fig.cap="Variation of the p-score per month for the year 2020", fig.pos='h'}
fig_2
```

```{r fig.cap="Average number of monthly deaths in 2011-2019 compared to monthly death in 2020", fig.pos='h', fig.height=3.5, fig.width=6}
fig_1
```

```{r fig.cap="Average number of monthly deaths in 2011-2019 compared to monthly death in 2020 by age group", fig.pos='h', fig.height=4.5, fig.width=7}
fig1_01
```

```{r fig.cap="Average number of monthly deaths in 2011-2019 compared to monthly death in 2020 by gender", fig.pos='h', fig.height=4.5, fig.width=7}
fig1_02
```

```{r fig.cap="Autocorrelation function (ACF) of deaths from 2011 to 2019", fig.pos='h', fig.height=3.3, fig.width=6}
fig_3_2020
```

```{r fig.cap="Estimation of the trend component of deaths", fig.pos='h'}
fig_4
```

```{r fig.cap="Daily confirmed cases of COVID19 up to today (7-day average)", fig.pos='h'}
fig_n
```
